// console.log("Hello");

// To store a value
localStorage.setItem('username', 'Keira');

// To retrieve a value
let value = localStorage.getItem('username');
console.log(value);

let input = document.getElementById('input');
let button = document.getElementById('button');
let retrieve = document.getElementById('retrieve');
let output = document.getElementById('output');

button.addEventListener('click', function (event){
    //console.log(event);
    console.log(input.value);
    let content = localStorage.getItem('input');
    content += ',' + input.value;
    console.log(content.split(','));
    localStorage.setItem('input', content);
})

retrieve.addEventListener('click', function (event){
    output.value = localStorage.getItem('input');
})

localStorage.clear();

function buffer(){
    localStorage.setItem('name', 'Emilie, Sebastien');
    localStorage.setItem('num_clicks', '5, 6');
    localStorage.setItem('time', '40, 10');
}

// This is a parallel universe - Earth-2